package com.sda.animals;

public class Cow extends Animal {

    @Override
    public String makeASound(){
        return "MOO!";
    }
}
