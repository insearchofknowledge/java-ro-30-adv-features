package com.sda.animals;

public abstract class Animal {

    public abstract String makeASound();
}
