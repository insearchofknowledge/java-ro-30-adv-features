package com.sda.animals;

public class Main {

    public static void main(String[] args) {

        Dog d = new Dog();
        Cow c = new Cow();

        System.out.println(d.makeASound());
        System.out.println(c.makeASound());

        Animal[] animals= new Animal[5];

        animals[0]=new Dog();
        animals[1]=new Cow();
        animals[2]=new Cat();
        animals[3]=new Dog();
        animals[4]=new Cow();

        for(int i=0;i<animals.length;i++){
            System.out.println(animals[i].makeASound());
        }



    }
}
