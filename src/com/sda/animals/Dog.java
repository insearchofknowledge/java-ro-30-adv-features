package com.sda.animals;

public class Dog extends Animal {

    @Override
    public String makeASound(){
        return "Woof!";
    }

}
