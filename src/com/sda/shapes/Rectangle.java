package com.sda.shapes;

import com.sda.shapes.enums.ShapeType;

public class Rectangle extends Shape {

    public int height;

    public Rectangle(int base, String color, int height) {
        super(base, color, ShapeType.RECTANGLE);
        this.height = height;
    }

    @Override
    public Integer getArea(){
        return base*height;
    }

    public String rectangleToString(){
        return "I am a square. "+super.toString();
    }
}
