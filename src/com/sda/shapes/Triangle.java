package com.sda.shapes;

import com.sda.shapes.enums.ShapeType;

public class Triangle extends Shape {

    public int height;

    public Triangle(int height, int base) {
        super(base);
        this.height = height;
    }
    public Triangle(int height, int base, String color){
        super(base, color,ShapeType.TRIANGLE);
        this.height=height;
    }

    @Override
    public Integer getArea() {
        return (height * super.base) / 2;
    }

    public String triangleToString(){
        return"I am triangle. " +super.toString();
    }
}
