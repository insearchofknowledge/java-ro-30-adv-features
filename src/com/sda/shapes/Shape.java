package com.sda.shapes;

import com.sda.shapes.enums.ShapeType;

/*
Shape - getArea
Triunghi - baza inaltime
Patrat - baza
dreptunghi - baza inaltime
 */
public class Shape {
    public int base = 0;
    public String color;
    public ShapeType shapeType;

    public Shape(int base) {
        this.base = base;
    }

    public Shape(int base, String color, ShapeType shapeType) {
        this.base = base;
        this.color = color;
        this.shapeType=shapeType;
    }

    public Integer getArea() {
        return null;
    }

    public String toString() {
        return "I am " + color + " shape with a base of " + base + " and area of " + getArea() + ". ";
    }
}
