package com.sda.shapes;

import com.sda.shapes.enums.ShapeType;

public class Square extends Shape {

    public Square(int base){
        super(base);
    }

    public Square(int base, String color){
        super(base,color, ShapeType.SQUARE);
    }
    @Override
    public Integer getArea(){
        return (base*base);
    }

    public String squareToString(){
        return"I am a square. " +super.toString();
    }
}
