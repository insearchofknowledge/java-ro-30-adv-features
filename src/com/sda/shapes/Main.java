package com.sda.shapes;

public class Main {
    public static void main(String[] args) {

        Triangle triangle1 = new Triangle(10, 3, "orange");
        System.out.println(triangle1.triangleToString());

        Square square1 = new Square(10, "green");
        System.out.println(square1.squareToString());

        Rectangle rectangle1 = new Rectangle(10, "red", 15);
        System.out.println(rectangle1.rectangleToString());


    }
}
