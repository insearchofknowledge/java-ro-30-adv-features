package com.sda.shapes.enums;

public enum ShapeType {

    TRIANGLE, RECTANGLE, SQUARE, CIRCLE
}
