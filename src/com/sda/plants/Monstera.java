package com.sda.plants;

public class Monstera extends Plant {

    public Monstera(String name, int acquisitionPrice){
        this.setName(name);
        this.setAcquisitionPrice(acquisitionPrice);
    }

    @Override
    int getSellingPrice() {
        return getAcquisitionPrice()*2;
    }
}
