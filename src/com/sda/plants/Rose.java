package com.sda.plants;

public class Rose extends Plant{

    public Rose(String name, int acquisitionPrice){
        this.setName(name);
        this.setAcquisitionPrice(acquisitionPrice);
    }

    @Override
    int getSellingPrice() {
        return getAcquisitionPrice()*2;
    }
}
