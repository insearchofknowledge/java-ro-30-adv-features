package com.sda.shapes_abstract;

public class Rectangle extends Shapes {
    private double length;
    private double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public String getName() {
        return "Rectangle";
    }

    public Double getArea() {
        return length * width;
    }
}
