package com.sda.shapes_abstract;

public class Circle extends Shapes {

    private double raza;

    public Circle(double raza) {
        this.raza = raza;

    }

    public void setRaza(double raza) {
        this.raza = raza;
    }

    @Override
    public String getName() {
        return "Circle";
    }

    @Override
    public Double getArea() {
        return 3.14 * raza * raza;
    }
}
