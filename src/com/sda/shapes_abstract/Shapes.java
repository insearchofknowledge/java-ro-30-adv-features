package com.sda.shapes_abstract;

public abstract class Shapes {

    public abstract String getName();

    public abstract Double getArea();

    public void printInfo() {
        System.out.println(getName() + " with an area of " + getArea());
    }

}
