package com.sda.shapes_abstract;

public class Main {
    public static void main(String[] args) {

        Shapes[] shapes = new Shapes[4];
        shapes[0] = new Circle(10);
        shapes[1] = new Triangle(10, 20);
        shapes[2] = new Rectangle(10, 15);
        shapes[3] = new Circle(20);

        // null.metoda sau null.variabila sau null.orice

        for (int i = 0; i < shapes.length; i++) {
            shapes[i].printInfo();
        }

    }
}
