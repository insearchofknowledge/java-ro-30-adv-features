package com.sda.shapes_abstract;

public class Triangle extends Shapes {

    private double base;
    private double height;

    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String getName() {
        return "Triangle";
    }

    @Override
    public Double getArea() {
        return base * height / 2;
    }
}
