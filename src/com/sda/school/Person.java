package com.sda.school;

import com.sda.school.exceptions.InvalidAgeException;

public class Person {

    private Long cnp;
    private String firstName;
    private String lastName;
    private int age;

    public Person() {

    }

    public Person(Long cnp, String firstName, String lastName, int age) {
        if (age <0){
            throw new InvalidAgeException(age);
        }
        this.cnp = cnp;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public Person(Long cnp) {
    }

    public Long getCnp() {
        return cnp;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "My name is " + firstName + " " + lastName + ". I'm " + age + " years old.";
    }

}



