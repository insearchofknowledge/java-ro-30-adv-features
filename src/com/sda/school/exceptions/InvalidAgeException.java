package com.sda.school.exceptions;

public class InvalidAgeException extends RuntimeException{
    public InvalidAgeException(int wrongAge){
        super("Wrong value of age entered:" +wrongAge + ". It should be a positive number");
    }
}
