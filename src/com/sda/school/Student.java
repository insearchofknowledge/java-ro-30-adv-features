package com.sda.school;

public class Student extends Person {

    private double examGrade;

    public Student(Long cnp, String firstName, String lastName, int age, double examGrade) {
        super(cnp, firstName, lastName, age);
        this.examGrade = examGrade;
    }

    public double getExamGrade() {
        return this.examGrade;
    }

    public void setExamGrade(double examGrade) {
        this.examGrade = examGrade;
    }

    public String toString() {
        return "Hi i am student." + super.toString() + " My average is " + examGrade + ".";
    }

}
