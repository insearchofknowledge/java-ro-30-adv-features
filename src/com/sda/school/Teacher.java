package com.sda.school;


import com.sda.school.enums.Subject;

public class Teacher extends Person {

    // English Math Chemistry Romanian
    private Enum subject;

    public Teacher(Long cnp, String firstName, String lastName, int age, String subject) {
        super(cnp, firstName, lastName, age);
        this.subject = Subject.valueOf(subject);
    }

    public Teacher(Long cnp, String subject) {
        super(cnp);
        this.subject = Subject.valueOf(subject);
    }

    @Override
    public String toString() {
        return "Hi I am teaching " + subject + ". " + super.toString();
    }
}
