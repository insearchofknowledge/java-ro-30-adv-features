package com.sda.school.enums;

public enum Occupation {
    DIRECTOR, JANITOR, GUARD, PROFESSOR
}
