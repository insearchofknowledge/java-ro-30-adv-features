package com.sda.school.enums;

public enum Subject {

    ENGLISH, MATH, CHEMISTRY, ROMANIAN
}
