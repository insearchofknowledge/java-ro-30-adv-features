package com.sda.school;

import com.sda.school.enums.Occupation;

public class Employee extends Person {

    private Occupation occupation;

    public Employee(Long cnp, String firsName, String lastName, int age, Occupation occupation) {
        super(cnp, firsName, lastName, age);
        this.occupation = occupation;
    }

    public Employee(Long cnp, Occupation occupation) {
        super(cnp);
        this.occupation = occupation;
    }

    public String toString() {
        return "Hi i am the " + occupation + " " + super.toString();
    }

}
