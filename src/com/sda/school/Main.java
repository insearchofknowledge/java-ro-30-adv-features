package com.sda.school;

import com.sda.school.Employee;
import com.sda.school.Student;
import com.sda.school.Teacher;
import com.sda.school.enums.Occupation;
import com.sda.school.enums.Subject;

public class Main {

    public static void main(String[] args) {

        Student s1 = new Student(1292546L, "", "Alex", 20, 10.0);
        Teacher t1 = new Teacher(1234578999l, "Maria", "Popescu", 22, "ENGLISH");
        Employee e1 = new Employee(1925874444L, "Alex", "Popescu", 36, Occupation.DIRECTOR);

        System.out.println(t1.toString());
        System.out.println(s1.toString());
        System.out.println(e1.toString());

    }
}




